// Setting prototypes
Array.prototype.findReg = function(match) {
    var reg = new RegExp(match);

    return this.filter(function(item){
        return typeof item == 'string' && item.match(reg);
    });
}

// Name of the script
var moduleName = "WCM"

// Loading pack variables
//const packData = require("./package.json")

// Setting a color scheme for console.log
const colorScheme = require(__dirname + "/utils/colorScheme.js")
console.log('\x1b[1m') // Setting color pre-settings

const DroidsDebug = require("droid-debug")
// Enabling debug mode
var debugHand = new DroidsDebug(0)
if (process.argv.indexOf("debug") != -1) {
	debugHand = new DroidsDebug(1)
} else if (process.argv.indexOf("killme") != -1) {
	debugHand = new DroidsDebug(2)
} else if (process.argv.indexOf("devops") != -1) {
	debugHand = new DroidsDebug(3)
}

// Flags

// Setting flags
// Example:
/* if (process.argv.indexOf("--api-disable")) {
	apiDisable = true
} */

// Setting `debugHand` variables
debugHand.setPostErrorMsgSwitch(true)

// Module loading
const wallpaper = require("wallpaper")
debugHand.log("Module 'wallpaper' loaded as 'wallpaper'.", 1, moduleName)
const request = require("request")
debugHand.log("Module 'request' loaded as 'request'.", 1, moduleName)
const fs = require("fs")
debugHand.log("Module 'fs' loaded as 'fs'.", 1, moduleName)

// Variables
var config = null

// Loading config
let triedAgain = false
while (true) {
	let stop = false
	try {
		config = require(__dirname + "/config.js")
		stop = true
	} catch (e) {
		if (triedAgain) {
			debugHand.error("Config couldn't be loaded. Please make a manual copy of 'config.js.example' and name it 'config.js'.", moduleName)
			process.exit(2)
		} else {
			if (fs.existsSync(__dirname + "/config.js")) {
				debugHand.error("Config file seems to be corrupt. Please remove or fix the file.", moduleName)
				process.exit(4)
			} else {
				// TODO: Make setup screen
				debugHand.error("Config file couldn't be found. Creating config from example file...", moduleName)
				fs.copyFileSync(__dirname + '/config.js.example', __dirname + '/config.js')
				triedAgain = true
			}
		}
	} finally {
		if (stop) {
			debugHand.log("Config file loaded!", 0, moduleName)
			break
		}
	}
}

if (!fs.existsSync(config.options.downloadLocation)) {
	debugHand.log("Wallpaper download location folder doesn't seem to exist but no worries we'll make it exist!", 1, moduleName)
	fs.mkdirSync(config.options.downloadLocation)
	if (!fs.existsSync(config.options.downloadLocation)) {
		debugHand.error("Wallpaper download location folder didn't exist and couldn't be created. Exiting...", moduleName)
		process.exit(5)
	}
}

// Functions
function argumentsExist(argArr) {
	for (let i = 0; i < argArr.length; i++) {
		if (argArr[i] == null) {
			return false
		}
	}
	return true
}

function isEmpty(variable) {
	if (variable == null) {
		return true
	}
	if (typeof variable == "string") {
		if (variable == "") {
			return true
		}
	}
	return false
}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

function getRandomInt(Low, High) {
	return Math.floor(Math.random() * (1 + High - Low)) + Low
}

function getRandom(array) {
	return array[getRandomInt(0, array.length-1)]
}

function isKnownPic(source, subSource, picPostID) {
	let moduleName = "isKnownPic()"
	let wallpaperFolder = fs.readdirSync(config.options.downloadLocation)
	debugHand.log("Wallpaper folder: ", 3, moduleName, wallpaperFolder)
	let wallpaperMatch = wallpaperFolder.findReg("^" + source + "_" + subSource + "_" + picPostID + "\..*$")
	debugHand.log("Found following wallpapers: ", 2, moduleName, wallpaperMatch)
	debugHand.log("isEmpty(wallpaperMatch[0])? " + (isEmpty(wallpaperMatch[0])), 2, moduleName)
	if (isEmpty(wallpaperMatch[0])) {
		return false
	} else {
		return true
	}
}

function getCompatibility(source, array, subsource) {
	switch (source) {
		case "reddit":
			let compImages = []
			while (array[0] != null) {
				let picID = getRandomInt(0, array.length-1)
				debugHand.log("PICK A CARD!", 3, moduleName, picID)
				let picturePost = array[picID].data
				debugHand.log("IT'S A ...", 3, moduleName, picturePost.id)
				let stop = false
				// Checking if post is an image post
				debugHand.log("Post is image " + (picturePost.post_hint != "image"), 3, moduleName)
				if (picturePost.post_hint != "image") {
					stop = true
				}
				// Checking (if set) if picture is a known picture
				if (!stop && config.options.unknownPicsOnly) {
					debugHand.log("Checking if picture is known...", 3, moduleName)
					if (isKnownPic(source, subsource, picturePost.id)) {
						stop = true
					}
				}
				let picOrientation = null
				// Checking if picture is horizontal or vertical
				debugHand.log("Getting orientation of picture...", 3, moduleName)
				if (!stop && picturePost.preview.images[0].source.width > picturePost.preview.images[0].source.height) {
					picOrientation = "horizontal"
				} else if (!stop && picturePost.preview.images[0].source.width < picturePost.preview.images[0].source.height) {
					picOrientation = "vertical"
				} else {
					stop = true
				}
				// Checking (if set) if picture orientation is allowed
				debugHand.log("Checking if picture orientation is accepted...", 3, moduleName)
				if (!stop && !config.accept.orientation[picOrientation]) {
					stop = true
				}
				// Checking (if set) if picture is low resolution compared to the set resolution
				if (!stop && !config.accept.lowResolution) {
					debugHand.log("Checking for low resolution...", 3, moduleName)
					if (picturePost.preview.images[0].source.width <= config.screenResolution.width) {
						stop = true
					} else if (picturePost.preview.images[0].source.height <= config.screenResolution.height) {
						stop = true
					}
				}
				debugHand.log("Is it stopping time? " + stop, 3, moduleName)
				if (!stop) {
					debugHand.log("Saving following array: ", 3, moduleName, picturePost)
					compImages.push(picturePost)
				}
				array.splice(picID, 1)
				array.filter(val => val)
				debugHand.log("array[0] != null seems to be " + (array[0] != null), 3, moduleName)
			}
			return compImages
		default:
			return -1
	}
}

// Test code
if (process.argv.indexOf("--test") != -1) {
	let moduleName = "Test Code"
	let source = "reddit"
	let subreddit = getRandom(config.sourcing[source])
	debugHand.log("We are doing subreddit r/" + subreddit, 2, moduleName)
	request.get("https://www.reddit.com/r/" + subreddit + "/hot.json?limit=100", null, (error, response, body) => {
		if (!error && response.statusCode == 200) {
			let redRes = JSON.parse(body)
			let redPostsRes = redRes.data.children
			debugHand.log("Got the " + redPostsRes.length + " children!", 3, moduleName)
			let compList = getCompatibility(source, redPostsRes, subreddit)
			debugHand.log("Compatibility list: ", 1, moduleName, compList)
			debugHand.log("Compatibility list length: ", 1, moduleName, compList.length)
			process.exit(0)
		} else {
			debugHand.error("Reddit returned an error: ", moduleName, error)
			debugHand.error("Their response: ", moduleName, response)
			debugHand.error("With the following body: ", moduleName, body)
			process.exit(12)
		}
	})
} else {
	// Main code
	function main() {
		let moduleName = "main()"
		let source = getRandom(config.accept.sourcing)
		debugHand.log("Getting wallpapers from source: ", 2, moduleName, source)
		switch (source) {
			case "reddit":
				let subreddit = getRandom(config.sourcing[source])
				debugHand.log("We are doing subreddit r/" + subreddit, 2, moduleName)
				request.get("https://www.reddit.com/r/" + subreddit + "/hot.json?limit=100", null, (error, response, body) => {
					if (!error && response.statusCode == 200) {
						let redRes = JSON.parse(body)
						let redPostsRes = redRes.data.children
						debugHand.log("Got the " + redPostsRes.length + " children!", 3, moduleName)
						let compPicList = getCompatibility(source, redPostsRes, subreddit)
						if (compPicList.length <= 0) {
							// TODO: Change to new sub-source and if that is empty, new source
							debugHand.error("No picture could be found on the set source and sub-source. Exiting")
							process.exit(14)
						} else {
							let chosenPicObj = getRandom(compPicList)
							debugHand.log("Picture of choice: ", 2, moduleName, chosenPicObj)
							if (!config.options.keepOldWallpapers) {
								fs.readdir(config.options.downloadLocation, function(err, list) {
									if (err) {
										return debugHand.error("Removing old wallpapers listing threw error: ", moduleName, err)
									} else {
										list.forEach(function (file) {
											fs.unlink(config.options.downloadLocation + file, function(err2) {
												if (err2) {
													debugHand.error("Removing old wallpaper threw error: ", moduleName, err2)
												}
											})
										})
									}
								})
							}
							let picExt = chosenPicObj.url.substring(chosenPicObj.url.lastIndexOf("."), chosenPicObj.url.length)
							let downloadLoc = config.options.downloadLocation + source + "_" + subreddit + "_" + chosenPicObj.id + picExt
							debugHand.log("Download file to: ", 1, moduleName, downloadLoc)
							request.get(chosenPicObj.url)
							.pipe(fs.createWriteStream(downloadLoc))
							.on('finish', () => {
								wallpaper.set(downloadLoc).finally(() => {
									debugHand.log("Wallpaper downloaded and set!", 0)
								})
							})
							.on('error', (error) => {
								debugHand.error("Failed to download wallpaper.", moduleName, error)
								process.exit(13)
							})
						}
					} else {
						debugHand.error("Reddit returned an error: ", moduleName, error)
						debugHand.error("Their response: ", moduleName, response)
						debugHand.error("With the following body: ", moduleName, body)
						process.exit(12)
					}
				})
				break
			default:
				debugHand.error("No valid source selected. Exiting...", moduleName)
				process.exit(11)
				break
		}
	}
	
	async function repeat() {
		let timeoutTime = null
		switch (config.options.wallpaperChange.unit) {
			case "seconds":
				timeoutTime = config.options.wallpaperChange.time * 1000
				break;
			case "minutes":
				timeoutTime = config.options.wallpaperChange.time * 60 * 1000
				break;
			case "hours":
				timeoutTime = config.options.wallpaperChange.time * 60 * 60 * 1000
				break;
			default:
				debugHand.error("Invalid time unit set in config file.", moduleName)
				break;
		}
		debugHand.log("Changing wallpaper in " + timeoutTime + " milliseconds.", 0, moduleName)
		await sleep(timeoutTime)
		main()
	}
	
	async function mainCode() {
		await main()
		while (config.options.wallpaperChange.time != 0) {
			await repeat()
		}
	}
	mainCode()
}

//wallpaper.set("./Wallpapers/Test1.png")